from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User
from .models import ProductModel,CartModel
from django.contrib.auth.hashers import check_password

class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        required=True,
        min_length=5,
        max_length=15,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(required=True,min_length=2, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password']
        )
        return user





class LoginSerializer(serializers.Serializer):

    email = serializers.EmailField(
        required=True
    )
   
    password = serializers.CharField(
        required=True,
       
    )
    


    def validate(self, validated_data):
       

        user = User.objects.filter(email=validated_data["email"])
        print(user)
       
        if not user.exists():
            raise serializers.ValidationError({"email":"Email not found."})
       

        if not check_password(validated_data["password"],user.first().password):
            raise serializers.ValidationError({'password':"Password Incorrect"})
        
       
        print("before validated data")
        
        print(validated_data)
        return validated_data
class AllProductSerializer(serializers.ModelSerializer):
  
    class Meta:
        model = ProductModel
        fields = '__all__'


class ProductName(serializers.ModelSerializer):
    
    class Meta:
        model=ProductModel
        fields= "__all__"

class CartProductSerializer(serializers.ModelSerializer):
    product = ProductName()
    class Meta:
        model=CartModel
        fields=["product"]

class CartProductWishList(serializers.ModelSerializer):
    class Meta:
        model=ProductModel
        fields='__all__'

class WishlistSerializer(serializers.Serializer):
    product_id = serializers.IntegerField(required=True)

    def validate(self,validated_data):

        if not ProductModel.objects.filter(id=validated_data["product_id"]).exists():
            raise serializers.ValidationError({"response":"invalid_product_id"})

        return validated_data
    
    def create(self,validated_data):
        request = self.context.get("request")
        product = ProductModel.objects.get(id=validated_data["product_id"])
        wishlist = CartModel.objects.create(
            product = product,
            user = request.user
        )
        return validated_data
class DeleteFromWishListSerializer(serializers.Serializer):
    product_id = serializers.IntegerField(required=True)

    def validate(self,validated_data):
        print("came to validate")
        if not CartModel.objects.filter(product_id=validated_data["product_id"]).exists():
            print("product does not exist")
            raise serializers.ValidationError({"response":"invalid_product_id"})
        print(validated_data)
        return validated_data
  

