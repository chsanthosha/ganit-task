from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class ProductModel(models.Model):
    Productimage=models.ImageField(upload_to='products')
    productname=models.CharField(max_length=255)
    productprice=models.CharField(max_length=32)
    

class CartModel(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,related_name='Userdata')
    product=models.ForeignKey(ProductModel,on_delete=models.CASCADE,related_name='cardata')
    

